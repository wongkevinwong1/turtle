﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Turtle.Client
{
    public class Turtle : ITurtle
    {
        private int _x, _y;
        private DirectionEnum _direction;
        private readonly int _maxX;
        private readonly int _maxY;

        public Turtle(int maxX, int maxY)
        {
            this._maxX = maxX;
            this._maxY = maxY;

            _x = 0;
            _y = 0;
            _direction = DirectionEnum.NONE;
        }

        public void Move(string direction)
        {
            var nextX = _x;
            var nextY = _y;
            switch (direction.ToUpper())
            {
                case "RIGHT":
                    Place(1, 0, DirectionEnum.WEST);
                    break;

                case "LEFT":
                    Place(-1, 0, DirectionEnum.EAST);
                    break;

                default:
                    if (_direction == DirectionEnum.EAST)
                    {
                        Place(-1, 0, DirectionEnum.EAST);
                    }
                    else if (_direction == DirectionEnum.WEST)
                    {
                        Place(1, 0, DirectionEnum.WEST);
                    }
                    else if (_direction == DirectionEnum.NORTH)
                    {
                        Place(0, 1, DirectionEnum.NORTH);
                    }
                    else if (_direction == DirectionEnum.SOUTH)
                    {
                        Place(0, -1, DirectionEnum.SOUTH);
                    }
                    break;
            }
        }

        public void Place(int deltaX, int deltaY, DirectionEnum direction)
        {
            var nextX = _x + deltaX;
            var nextY = _y + deltaY;

            if (nextX <= _maxX && nextY <= _maxY)
            {
                _x = nextX;
                _y = nextY;
                _direction = direction;
            }
        }

        public Point Report()
        {
            return new Point { X = _x, Y = _y, Direction = _direction };
        }
    }
}
