﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Turtle.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = "";
            ITurtle turtle = new Turtle(5,5);

            var parser = new TurtleCommandParser(turtle);

            Console.WriteLine("---INPUT---");

            while (( input = Console.ReadLine())!=null)
            {
                parser.Command(input);

                if (parser.restart)
                {
                    turtle = new Turtle(5, 5);
                    parser = new TurtleCommandParser(turtle);
                    Console.WriteLine("---INPUT---");
                }
            }
        }
    }
}
