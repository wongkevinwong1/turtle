﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Turtle.Client
{
    public class TurtleCommandParser
    {
        private readonly ITurtle _turtle;

        public bool restart
        {
            get;
            private set;
        }

        public TurtleCommandParser(ITurtle turtle)
        {
            this._turtle = turtle;
            restart = false;
        }
        public void Command(string command)
        {
            if (!string.IsNullOrEmpty(command))
            {
                var subCommands = command.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                switch (subCommands[0].ToUpper())
                {
                    case "PLACE":
                        var parameters = subCommands[1].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        _turtle.Place(int.Parse(parameters[0]), int.Parse(parameters[1]), (DirectionEnum)Enum.Parse(typeof(DirectionEnum), parameters[2],true));
                        restart = false;
                        break;
                    case "LEFT":
                        _turtle.Move("LEFT");
                        restart = false;
                        break;

                    case "MOVE":
                        _turtle.Move("");
                        restart = false;
                        break;

                    case "RIGHT":
                        _turtle.Move("RIGHT");
                        restart = false;
                        break;
                    case "REPORT":
                        var point = _turtle.Report();
                        Console.WriteLine();
                        Console.WriteLine("---OUTPUT---");
                        Console.WriteLine(string.Format("{0},{1},{2}", point.X, point.Y,point.Direction.ToString()));
                        Console.WriteLine();

                        restart = true;
                        break;
                }
            }
        }
    }
}
