﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Turtle.Client
{
    public interface ITurtle
    {
        void Place(int x, int y, DirectionEnum direction);
        void Move(string direction);
        Point Report();
    }
}
