﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Turtle.Client
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public DirectionEnum Direction { get; set; }
    }
}
